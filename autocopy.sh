#!/bin/bash

# this script automatically copies the jar as maven creates it
# call it using & to send it in the background like this
# ./autocopy.sh /path/to/server/plugins/folder &

srcfile='target/nopipes-1.1.jar'
destdir="$1"

cd "$(dirname "$0")"

inotifywait -q -m -e close_write "$srcfile" | while read -r filename event
do
	sleep 2
	cp "$srcfile" "$destdir/" && echo "Copied JAR"
done

