package xyz.lucaci32u4.nopipes;

import xyz.lucaci32u4.command.SimpleCommandExecutor;
import xyz.lucaci32u4.command.SubcommandHandler;
import xyz.lucaci32u4.command.parser.BooleanParser;
import xyz.lucaci32u4.command.parser.IntegerParser;
import xyz.lucaci32u4.command.parser.StringParser;
import xyz.lucaci32u4.command.reader.ParameterMap;
import org.bukkit.*;
import org.bukkit.block.Barrel;
import org.bukkit.block.BlockState;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.lucaci32u4.nopipes.json.JSONArray;
import xyz.lucaci32u4.nopipes.json.JSONException;
import xyz.lucaci32u4.nopipes.json.JSONObject;
import xyz.lucaci32u4.nopipes.pipes.PipeGroup;
import xyz.lucaci32u4.nopipes.pipes.PipeInput;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class NoPipes extends JavaPlugin implements Listener {
    private static final Path saveFolder = Paths.get("./plugins/NoPipes");
    private static final Path savePipesFile = saveFolder.resolve("pipes.json");
    public static final String CHAT_COLOR = ChatColor.GREEN.toString();
    public static final String CHAT_PIPE = ChatColor.GRAY.toString();
    private static final int defaultTransferChunk = 2; // stacks
    private static final int defaultTransferStackSize = 64; // items

    PlayerWandManager playerWandManager = new PlayerWandManager();
    Map<String, PipeGroup> pipeGroups = new HashMap<>();

    @EventHandler
    public void onItemClickEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getItem() == null) {
            return;
        }
        if (!playerWandManager.hasPlayer(player)) {
            if (PlayerWandState.isWand(event.getItem())) {
                player.getInventory().remove(event.getItem());
            }
            return;
        }
        if (!PlayerWandState.isWand(event.getItem())) {
            return;
        }
        if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
            BlockState block = event.getClickedBlock().getState();
            PlayerWandState playerWand = playerWandManager.getPlayer(player);
            if (block instanceof Barrel) {
                playerWand.interactWith((Barrel) block, () -> pipeGroups.values().stream());
            }
            event.setCancelled(true);
        }
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            PlayerWandState state = playerWandManager.getPlayer(player);
            state.setFunction(state.getFunction().next());
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent event) {
        if (event.isNewChunk()) {
            return;
        }
        Chunk chunk = event.getChunk();
        int chunkBeginX = chunk.getX() * 16;
        int chunkBeginZ = chunk.getZ() * 16;
        int chunkEndX = chunkBeginX + 16, chunkEndZ = chunkBeginZ + 16;
        pipeGroups.values().forEach(pg -> pg.syncPipesIf(p -> {
            Location loc = p.getLocation();
            return chunkBeginX <= loc.getBlockX() && loc.getBlockX() < chunkEndX && chunkBeginZ <= loc.getBlockZ() && loc.getBlockZ() < chunkEndZ;
        }));
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Location location = event.getBlock().getLocation();
        if (event.getBlock().getState() instanceof Barrel) {
            pipeGroups.values().forEach(g -> g.removePipe(location));
        }
    }

    @EventHandler
    public void onPlayerLeaveEvent(PlayerQuitEvent event) {
        if (playerWandManager.hasPlayer(event.getPlayer())) {
            playerWandManager.remove(event.getPlayer());
            cleanGroups();
        }
    }

    @EventHandler
    public void onInstantItemTransfer(InventoryMoveItemEvent event) {
        InventoryHolder destinationHolder = event.getDestination().getHolder();
        InventoryHolder sourceHolder = event.getSource().getHolder();
        if (destinationHolder instanceof Barrel) {
            Barrel barrel = (Barrel) destinationHolder;
            Location location = barrel.getLocation();
            getServer().getScheduler().scheduleSyncDelayedTask(this, () -> pipeGroups.values().forEach(g -> g.syncFiltersAt(location, barrel)), 10);
        }
        if (sourceHolder instanceof Barrel) {
            Barrel barrel = (Barrel) sourceHolder;
            Location location = barrel.getLocation();
            getServer().getScheduler().scheduleSyncDelayedTask(this, () -> pipeGroups.values().forEach(g -> g.syncFiltersAt(location, barrel)), 10);
        }

    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        InventoryHolder inventoryHolder = event.getInventory().getHolder();
        if (inventoryHolder instanceof Barrel) {
            Barrel barrel = (Barrel) inventoryHolder;
            Location location = barrel.getLocation();
            pipeGroups.values().forEach(g -> g.syncFiltersAt(location, barrel));
        }
    }

    @Override
    public void onEnable() {
        super.onEnable();
        getServer().getPluginManager().registerEvents(this, this);
        SimpleCommandExecutor.build().name("nopipes")
                .subcommand().name("enable-wand").explicitParameters(false)
                .parameter("pipe", new StringParser("", () -> pipeGroups.values().stream().filter(PipeGroup::isPublicVisibility).map(PipeGroup::getName)))
                .endSubcommand().subcommand().name("disable-wand").explicitParameters(false)
                .endSubcommand().subcommand().name("change-pipe").explicitParameters(false)
                .parameter("pipe", new StringParser("", () -> pipeGroups.values().stream().filter(PipeGroup::isPublicVisibility).map(PipeGroup::getName)))
                .endSubcommand().subcommand().name("transfer-speed").explicitParameters(false)
                .parameter("speed", new IntegerParser(defaultTransferChunk))
                .endSubcommand().subcommand().name("stack-size").explicitParameters(false)
                .parameter("size", new IntegerParser(defaultTransferStackSize))
                .endSubcommand().subcommand().name("info").explicitParameters(false)
                .parameter("pipe", new StringParser("", () -> pipeGroups.values().stream().filter(PipeGroup::isPublicVisibility).map(PipeGroup::getName)))
                .endSubcommand().subcommand().name("visibility").explicitParameters(false)
                .parameter("visibility", new BooleanParser(true, "public", "hidden"))
                .endSubcommand()
                .endCommand()
                .setHandler(new CommandHandler(), CommandHandler.class)
                .selfInstall(this, true);
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new TickHandler(), 0, 10);
        loadPipes();
    }

    public class CommandHandler {

        @SubcommandHandler("enable-wand")
        public void onEnableWand(CommandSender sender, ParameterMap param) {
            String group = param.get("pipe", String.class);
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (group == null || group.equals("")) {
                    player.sendMessage(ChatColor.RED + "You need to specify a pipe name!");
                    return;
                }
                PlayerWandState pws = new PlayerWandState(player, ensurePipeGroupExists(group));
                if (pws.addWand()) {
                    playerWandManager.addPlayer(pws);
                    player.sendMessage(CHAT_COLOR + "You can now use the pipe wand. Current pipe: " + CHAT_PIPE + group);
                } else {
                    player.sendMessage(ChatColor.RED + "Your inventory is full! Please try again later");
                }
                cleanGroups();
            }
        }

        @SubcommandHandler("disable-wand")
        public void onDisableWand(CommandSender sender, ParameterMap param) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (playerWandManager.hasPlayer(player)) {
                    player.sendMessage(ChatColor.GREEN + "Pipe wand is now turned off");
                    playerWandManager.remove(player);
                }
            }
            if (sender instanceof ConsoleCommandSender) {
                playerWandManager.stream().filter(w -> w.getPlayer().isOnline()).forEach(p -> {
                    p.getPlayer().sendMessage(ChatColor.DARK_RED + "Console turned off pipe wand for all players");
                });
                playerWandManager.clearAll();
                sender.sendMessage("Turned off pipe wand for all players");
            }
        }

        @SubcommandHandler("change-pipe")
        public void onChangePipe(CommandSender sender, ParameterMap param) {
            String group = param.get("pipe", String.class);
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (group == null || group.equals("")) {
                    player.sendMessage(ChatColor.RED + "You need to specify a pipe name!");
                    return;
                }
                if (playerWandManager.hasPlayer(player)) {
                    playerWandManager.getPlayer(player).setPipeGroup(ensurePipeGroupExists(group));
                    player.sendMessage(CHAT_COLOR + "Switched to pipe " + CHAT_PIPE + group);
                } else {
                    sender.sendMessage(ChatColor.RED + "You need to enable the pipe wand first!");
                }
            }
        }

        @SubcommandHandler("transfer-speed")
        public void onChangeSpeed(CommandSender sender, ParameterMap param) {
            Integer speed = param.get("speed", Integer.class);
            if (speed == null || speed < 0) {
                sender.sendMessage(ChatColor.RED + "Speed must be a positive integer!");
                return;
            }
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (playerWandManager.hasPlayer(player)) {
                    PipeGroup pipeGroup = playerWandManager.getPlayer(player).getPipeGroup();
                    int old = pipeGroup.getTransferStackCount();
                    pipeGroup.setTransferStackCount(speed);
                    player.sendMessage(CHAT_COLOR + "Changed speed for pipe " + CHAT_PIPE + pipeGroup.getName() + CHAT_COLOR + " from " + CHAT_PIPE + old + CHAT_COLOR + " to " + CHAT_PIPE + speed);
                } else  {
                    sender.sendMessage(ChatColor.RED + "You need to enable the pipe wand first!");
                }
            }
        }

        @SubcommandHandler("stack-size")
        public void onChangeStackSize(CommandSender sender, ParameterMap param) {
            Integer size = param.get("size", Integer.class);
            if (size == null || size < 0) {
                sender.sendMessage(ChatColor.RED + "Size must be a positive integer!");
                return;
            }
            size = Math.min(size, defaultTransferStackSize);
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (playerWandManager.hasPlayer(player)) {
                    PipeGroup pipeGroup = playerWandManager.getPlayer(player).getPipeGroup();
                    int old = pipeGroup.getTransferStackSize();
                    pipeGroup.setTransferStackSize(size);
                    player.sendMessage(CHAT_COLOR + "Changed stack size for pipe " + CHAT_PIPE + pipeGroup.getName() + CHAT_COLOR + " from " + CHAT_PIPE + old + CHAT_COLOR + " to " + CHAT_PIPE + size);
                } else  {
                    sender.sendMessage(ChatColor.RED + "You need to enable the pipe wand first!");
                }
            }
        }

        @SubcommandHandler("visibility")
        public void onChangeVisibility(CommandSender sender, ParameterMap param) {
            Boolean vis = param.get("visibility", Boolean.class);
            if (vis == null) {
                sender.sendMessage(ChatColor.RED + "Visibility must be either public or hidden!");
                return;
            }
            if (sender instanceof Player) {
                Player player = (Player) sender;
                if (playerWandManager.hasPlayer(player)) {
                    PipeGroup pipeGroup = playerWandManager.getPlayer(player).getPipeGroup();
                    boolean old = pipeGroup.isPublicVisibility();
                    pipeGroup.setPublicVisibility(vis);
                    player.sendMessage(CHAT_COLOR + "Changed visibility for pipe " + CHAT_PIPE + pipeGroup.getName() + CHAT_COLOR + " from " + CHAT_PIPE + old + CHAT_COLOR + " to " + CHAT_PIPE + vis);
                } else  {
                    sender.sendMessage(ChatColor.RED + "You need to enable the pipe wand first!");
                }
            }
        }

        @SubcommandHandler("info")
        public void onInfo(CommandSender sender, ParameterMap param) {
            String group = param.get("pipe", String.class);
            if (group == null || group.equals("")) {
                sender.sendMessage(ChatColor.RED + "You need to specify a pipe name!");
                return;
            }
            Optional<PipeGroup> pgr = pipeGroups.values().stream().filter(pg -> pg.getName().equals(group)).findAny();
            if (!pgr.isPresent()) {
                sender.sendMessage(ChatColor.RED + "No such pipe!");
                return;
            }
            sender.sendMessage(CHAT_COLOR + "Inspecting pipe " + CHAT_PIPE + pgr.get().getName() + CHAT_COLOR + " with speed " + CHAT_PIPE + pgr.get().getTransferStackCount() + CHAT_COLOR + " and stack size " + CHAT_PIPE + pgr.get().getTransferStackSize() + "\n" +
                    pgr.get().streamPipes().sorted(Comparator.comparingInt(p -> (p instanceof PipeInput ? -10 : 10) + (p.isFiltered() ? -1 : 1)))
                            .map(p -> CHAT_COLOR + "Found " + p.getDisplayName() + CHAT_COLOR + " at " + CHAT_PIPE + p.getLocation().getBlockX() + CHAT_COLOR + ", " + CHAT_PIPE + p.getLocation().getBlockY() + CHAT_COLOR + ", " + CHAT_PIPE + p.getLocation().getBlockZ())
                            .collect(Collectors.joining("\n"))
                    );
        }

    }

    private class TickHandler implements Runnable {
        @Override
        public void run() {
            pipeGroups.forEach((s, p) -> p.onTick());
        }
    }

    @Override
    public void onDisable() {
        storePipes();
        super.onDisable();
    }

    private PipeGroup ensurePipeGroupExists(String name) {
        PipeGroup group = pipeGroups.get(name);
        if (group == null) {
            group = new PipeGroup(name, defaultTransferChunk, defaultTransferStackSize, true);
        }
        pipeGroups.put(name, group);
        return group;
    }

    private void cleanGroups() {
        pipeGroups.entrySet().removeIf(e -> e.getValue().size() == 0 && playerWandManager.stream().noneMatch(pws -> pws.getPipeGroup() == e.getValue()));
    }

    private void loadPipes() {
        if (!Files.exists(saveFolder) || !Files.isDirectory(saveFolder) || !Files.exists(savePipesFile) || !Files.isRegularFile(savePipesFile)) {
            System.out.println("Loading default configuration");
            storePipes();
        }
        pipeGroups.clear();
        try {
            String jsonString = new String(Files.readAllBytes(savePipesFile));
            JSONArray array = new JSONArray(jsonString);
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject object = array.getJSONObject(i);
                    PipeGroup group = PipeGroup.fromJSON(object);
                    pipeGroups.put(group.getName(), group);
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                }
            }
        } catch (IOException exception) {
            System.out.println("Cannot read stored pipes");
            exception.printStackTrace();
        }
    }

    private void storePipes() {
        ensureDirectoryStructure();
        JSONArray array = pipeGroups.values().stream().map(PipeGroup::toJSON).collect(JSONArray::new, JSONArray::put, JSONArray::putAll);
        try {
            Files.write(savePipesFile, array.toString().getBytes());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void ensureDirectoryStructure() {
        try {
            if (Files.exists(saveFolder)) {
                if (!Files.isDirectory(saveFolder)) {
                    Files.delete(saveFolder);
                    Files.createDirectory(saveFolder);
                }
            } else {
                Files.createDirectory(saveFolder);
            }
            if (Files.exists(savePipesFile)) {
                if (!Files.isRegularFile(savePipesFile)) {
                    Files.delete(savePipesFile);
                }
            } else {
                Files.createFile(savePipesFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
