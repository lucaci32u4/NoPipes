package xyz.lucaci32u4.nopipes;

import org.bukkit.Location;
import org.bukkit.block.data.BlockData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum PipeFacing {
    UP(0, 1, 0, false),
    DOWN(0, -1, 0, false),
    NORTH(0, 0, -1, true),
    SOUTH(0, 0, 1, true),
    EAST(1, 0, 0, true),
    WEST(-1, 0, 0, true);

    private static final Pattern blockDataRegexFinder = Pattern.compile("facing=([a-z]+)");

    private final int offsetX;
    private final int offsetY;
    private final int offsetZ;
    private final boolean isHorizontal;

    PipeFacing(int offsetX, int offsetY, int offsetZ, boolean isHorizontal) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.offsetZ = offsetZ;
        this.isHorizontal = isHorizontal;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public int getOffsetZ() {
        return offsetZ;
    }

    public boolean isHorizontal() {
        return isHorizontal;
    }

    public Location offsetLocation(Location location) {
        return location.add(offsetX, offsetY, offsetZ);
    }

    public static PipeFacing getFromBlockData(BlockData blockData) {
        if (blockData == null) return null;
        Matcher matcher = blockDataRegexFinder.matcher(blockData.getAsString());
        if (matcher.find()) {
            return PipeFacing.valueOf(matcher.group(1).toUpperCase());
        }
        return null;
    }
}
