package xyz.lucaci32u4.nopipes;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class PlayerWandManager {
    private final Map<Player, PlayerWandState> playerWandState = new HashMap<>();

    public PlayerWandState getPlayer(Player player) {
        return playerWandState.get(player);
    }

    public boolean hasPlayer(Player player) {
        return playerWandState.containsKey(player);
    }

    public void addPlayer(PlayerWandState playerWandState) {
        this.playerWandState.put(playerWandState.getPlayer(), playerWandState);
    }

    public PlayerWandState remove(Player player) {
        PlayerWandState wand = playerWandState.remove(player);
        wand.removeWand();
        return wand;
    }

    public Stream<PlayerWandState> stream() {
        return playerWandState.values().stream();
    }

    public void clearAll() {
        playerWandState.forEach((p, pws) -> pws.removeWand());
        playerWandState.clear();
    }

}
