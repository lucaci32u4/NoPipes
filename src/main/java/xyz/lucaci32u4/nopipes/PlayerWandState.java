package xyz.lucaci32u4.nopipes;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Barrel;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import xyz.lucaci32u4.nopipes.pipes.PipeGroup;
import xyz.lucaci32u4.nopipes.pipes.PipeInput;
import xyz.lucaci32u4.nopipes.pipes.PipeOutput;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static xyz.lucaci32u4.nopipes.NoPipes.CHAT_COLOR;
import static xyz.lucaci32u4.nopipes.NoPipes.CHAT_PIPE;

public class PlayerWandState {
    private static final String WAND_NAME = CHAT_COLOR + ChatColor.BOLD.toString() + "Pipemaker:";
    private static final Material WAND_MATERIAL = Material.STICK;

    private final Player player;
    private WandFunction function;
    private PipeGroup pipeGroup;

    public PlayerWandState(Player player, PipeGroup pipeGroup) {
        this.player = player;
        this.pipeGroup = pipeGroup;
        function = WandFunction.INPUT;
    }

    public Player getPlayer() {
        return player;
    }

    public WandFunction getFunction() {
        return function;
    }

    public PipeGroup getPipeGroup() {
        return pipeGroup;
    }

    public void setFunction(WandFunction function) {
        if (this.function != function) {
            this.function = function;
            updateWand();
        }
    }

    public void setPipeGroup(PipeGroup pipeGroup) {
        this.pipeGroup = pipeGroup;
    }

    public void interactWith(Barrel barrel, Supplier<Stream<PipeGroup>> pipeGroupListSupplier) {
        if (function == WandFunction.INPUT) {
            if (checkPipeExists(barrel, pipeGroupListSupplier)) {
                player.sendMessage(ChatColor.RED + "This pipe already exists!");
                return;
            }
            PipeInput pipe = pipeGroup.addPipeInput(barrel);
            player.sendMessage(CHAT_COLOR + "Created a new " + pipe.getDisplayName() + CHAT_COLOR + " for pipe " + CHAT_PIPE + pipeGroup.getName());
        }
        if (function == WandFunction.OUTPUT) {
            if (checkPipeExists(barrel, pipeGroupListSupplier)) {
                player.sendMessage(ChatColor.RED + "This pipe already exists!");
                return;
            }
            PipeOutput pipe = pipeGroup.addPipeOutput(barrel);
            player.sendMessage(CHAT_COLOR + "Created a new " + pipe.getDisplayName() + CHAT_COLOR + " for pipe " + CHAT_PIPE + pipeGroup.getName());
        }
        if (function == WandFunction.REMOVE) {
            Location removeLocation = barrel.getLocation();
            pipeGroupListSupplier.get().forEach(g -> g.removeAndGetPipes(p -> p.getLocation().equals(removeLocation), p -> {
                player.sendMessage(CHAT_COLOR + "Removed a " + p.getDisplayName() + CHAT_COLOR + " from pipe " + CHAT_PIPE + g.getName());
            }));
        }
        if (function == WandFunction.INSPECT) {
            Location removeLocation = barrel.getLocation();
            AtomicBoolean foundPipe = new AtomicBoolean(false);
            pipeGroupListSupplier.get().forEach(pg -> pg.forEachPipes(p -> {
                if (p.getLocation().equals(removeLocation)) {
                    foundPipe.set(true);
                    player.sendMessage(CHAT_COLOR + "Clicked on " + p.getDisplayName() + CHAT_COLOR + " from pipe " + CHAT_PIPE + pg.getName() + CHAT_COLOR + " with speed " + CHAT_PIPE + pg.getTransferStackCount() + CHAT_COLOR + " and stack size " + CHAT_PIPE + pg.getTransferStackSize());
                }
            }));
        }
    }

    private boolean checkPipeExists(Barrel barrel, Supplier<Stream<PipeGroup>> pipeGroupListSupplier) {
        Location location = barrel.getLocation();
        return pipeGroupListSupplier.get().anyMatch(pg -> pg.streamPipes().anyMatch(p -> p.getLocation().equals(location)));
    }

    public boolean hasWandInHand() {
        return isWand(player.getInventory().getItemInMainHand());
    }

    public boolean hasWand() {
        for (ItemStack stack : player.getInventory().getContents()) {
            if (stack == null) continue;
            if (isWand(stack)) {
                return true;
            }
        }
        return false;
    }

    private void updateWand() {
        for (ItemStack stack : player.getInventory().getContents()) {
            if (isWand(stack)) {
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName(WAND_NAME + " " + function.getDisplayName());
                stack.setItemMeta(meta);
            }
        }
    }

    public void removeWand() {
        Inventory inventory = player.getInventory();
        ArrayList<ItemStack> removeItems = new ArrayList<>(inventory.getSize());
        for (ItemStack stack : inventory.getContents()) {
            if (isWand(stack)) {
                removeItems.add(stack);
            }
        }
        removeItems.forEach(inventory::remove);
        player.updateInventory();
    }

    public boolean addWand() {
        if (hasWand()) {
            return true;
        }
        int emptySlotNumber = player.getInventory().firstEmpty();
        if (emptySlotNumber == -1) {
            return false;
        }
        ItemStack wand = new ItemStack(WAND_MATERIAL);
        ItemMeta meta = wand.getItemMeta();
        meta.setDisplayName(WAND_NAME + " " + function.getDisplayName());
        wand.setItemMeta(meta);
        player.getInventory().setItem(emptySlotNumber, wand);
        return true;
    }

    public static boolean isWand(ItemStack item) {
        return (item != null && item.getType() == WAND_MATERIAL && item.hasItemMeta() && item.getItemMeta().getDisplayName().contains(WAND_NAME));
    }
}
