package xyz.lucaci32u4.nopipes;

import org.bukkit.ChatColor;

public enum WandFunction {
    INPUT(ChatColor.GOLD, "Input"),
    OUTPUT(ChatColor.BLUE, "Output"),
    INSPECT(ChatColor.AQUA, "Inspect"),
    REMOVE(ChatColor.RED, "Remove");

    private final String displayString;
    private final String colorString;

    WandFunction(ChatColor color, String displayString) {
        this.displayString = color + displayString;
        this.colorString = color.toString();
    }

    public WandFunction next() {
        switch (this) {
            case INPUT:
                return OUTPUT;
            case OUTPUT:
                return INSPECT;
            case INSPECT:
                return REMOVE;
            case REMOVE:
                return INPUT;
        }
        return INPUT;
    }

    public String getDisplayName() {
        return displayString;
    }

    public String getColorString() {
        return colorString;
    }
}
