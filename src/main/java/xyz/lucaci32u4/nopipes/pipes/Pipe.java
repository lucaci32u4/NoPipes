package xyz.lucaci32u4.nopipes.pipes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import xyz.lucaci32u4.nopipes.json.JSONArray;
import xyz.lucaci32u4.nopipes.json.JSONObject;
import xyz.lucaci32u4.nopipes.PipeFacing;

import java.util.*;
import java.util.function.Supplier;

public abstract class Pipe {
    protected final Location location;
    protected final PipeGroup group;
    protected Location target;
    protected PipeFacing facing;
    protected final Set<Material> allowedMaterials = new HashSet<>();

    protected Pipe(Location location, PipeFacing facing, PipeGroup group) {
        this.location = location;
        this.group = group;
        this.facing = facing;
        this.target = facing.offsetLocation(location.clone());
    }

    public Location getLocation() {
        return location;
    }

    public PipeGroup getGroup() {
        return group;
    }

    public PipeFacing getFacing() {
        return facing;
    }

    public Location getTarget() {
        return target;
    }

    public void setFacing(PipeFacing facing) {
        this.facing = facing;
        target = facing.offsetLocation(location.clone());
    }

    public boolean isFiltered() {
        return !allowedMaterials.isEmpty();
    }

    public void updateFilter(Inventory source) {
        allowedMaterials.clear();
        Arrays.stream(source.getContents()).filter(Objects::nonNull).map(ItemStack::getType).filter(m -> !m.isAir()).forEach(allowedMaterials::add);
    }

    public boolean passesFilter(Material material) {
        return !material.isAir() && (allowedMaterials.isEmpty() || allowedMaterials.contains(material));
    }

    public boolean isLoadedChunk() {
        World world = target.getWorld();
        if (world == null) {
            return false;
        }
        return world.isChunkLoaded(location.getBlockX() >> 4, location.getBlockZ() >> 4) && world.isChunkLoaded(target.getBlockX() >> 4, target.getBlockZ() >> 4);
    }

    public Container getTargetContainer() {
        Block pipeBlock = location.getBlock();
        if (pipeBlock.isBlockPowered() || pipeBlock.isBlockIndirectlyPowered()) {
            return null;
        }
        BlockState state = target.getBlock().getState();
        if (state instanceof Chest
                || state instanceof Barrel
                || state instanceof Dispenser
                || state instanceof Dropper
                || state instanceof ShulkerBox
                || state instanceof Hopper
                || state instanceof Furnace) {
            return (Container) state;
        }
        return null;
    }

    public abstract String getDisplayName();

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject()
                .put("x", location.getX())
                .put("y", location.getY())
                .put("z", location.getZ())
                .put("facing", facing.toString())
                .put("filter", allowedMaterials.stream().map(Material::toString).collect((Supplier<JSONArray>) JSONArray::new, JSONArray::put, JSONArray::putAll));
        if (location.getWorld() != null) obj.put("world", location.getWorld().getUID());
        return obj;
    }

    protected static <T extends Pipe> T initFromJSON(PipeConstructor<T> constructor, JSONObject object, PipeGroup group) {
        World world = null;
        if (object.has("world")) {
            String worldUID = object.getString("world");
            world = Bukkit.getWorld(UUID.fromString(worldUID));
        }
        if (world == null) {
            world = Bukkit.getWorlds().get(0);
        }
        Location location = new Location(world, object.getDouble("x"), object.getDouble("y"), object.getDouble("z"));
        PipeFacing facing = PipeFacing.valueOf(object.getString("facing"));
        T pipe = constructor.construct(location, facing, group);
        JSONArray array = object.getJSONArray("filter");
        for (int i = 0; i < array.length(); i++) {
            pipe.allowedMaterials.add(Material.valueOf(array.getString(i)));
        }
        return pipe;
    }

    interface PipeConstructor<T extends Pipe> {
        T construct(Location location, PipeFacing facing, PipeGroup group);
    }
}
