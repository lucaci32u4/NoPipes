package xyz.lucaci32u4.nopipes.pipes;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Barrel;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import xyz.lucaci32u4.nopipes.json.JSONArray;
import xyz.lucaci32u4.nopipes.json.JSONException;
import xyz.lucaci32u4.nopipes.json.JSONObject;
import xyz.lucaci32u4.nopipes.PipeFacing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class PipeGroup {
    private final String name;
    private final List<PipeInput> inputs = new ArrayList<>();
    private final List<PipeOutput> outputs = new ArrayList<>();
    private int transferStackCount;
    private int transferStackSize;
    private boolean publicVisibility;

    public PipeGroup(String name, int transferStackCount, int transferStackSize, boolean publicVisibility) {
        this.name = name;
        this.transferStackCount = transferStackCount;
        this.transferStackSize = transferStackSize;
        this.publicVisibility = publicVisibility;
    }

    public String getName() {
        return name;
    }

    public void setTransferStackCount(int transferStackCount) {
        this.transferStackCount = transferStackCount;
    }

    public int getTransferStackCount() {
        return transferStackCount;
    }

    public void setTransferStackSize(int transferStackSize) {
        this.transferStackSize = transferStackSize;
    }

    public int getTransferStackSize() {
        return transferStackSize;
    }

    public boolean isPublicVisibility() {
        return publicVisibility;
    }

    public void setPublicVisibility(boolean publicVisibility) {
        this.publicVisibility = publicVisibility;
    }

    public PipeInput addPipeInput(Barrel barrel) {
        PipeFacing facing = PipeFacing.getFromBlockData(barrel.getBlockData());
        Location location = barrel.getLocation();
        if (isPipe(location)) {
            removePipe(location);
        }
        PipeInput pipe = new PipeInput(barrel.getLocation(), facing, this);
        pipe.updateFilter(barrel.getInventory());
        inputs.add(pipe);
        return pipe;
    }

    public PipeOutput addPipeOutput(Barrel barrel) {
        PipeFacing facing = PipeFacing.getFromBlockData(barrel.getBlockData());
        Location location = barrel.getLocation();
        if (isPipe(location)) {
            removePipe(location);
        }
        PipeOutput pipe = new PipeOutput(barrel.getLocation(), facing, this);
        pipe.updateFilter(barrel.getInventory());
        if (pipe.isFiltered()) {
            outputs.add(0, pipe);
        } else {
            outputs.add(pipe);
        }
        return pipe;
    }

    public void onTick() {
        int outputPipesSize = outputs.size();
        inputs.forEach(in -> {
            Container sourceContainer = in.getTargetContainer();
            if (sourceContainer == null) {
                return;
            }
            Inventory source = sourceContainer.getInventory();
            if (source.isEmpty()) {
                return;
            }
            int maxTransferStacks = transferStackCount;
            int sourceSize = source.getSize();
            int sourceIndex = 0;
            while (maxTransferStacks != 0 && sourceIndex < sourceSize) {
                // Find a stack to transfer
                ItemStack stack = null;
                while (sourceIndex < sourceSize) {
                    stack = source.getItem(sourceIndex);
                    if (stack != null && !stack.getType().isAir() && (!in.isFiltered() || in.passesFilter(stack.getType()))) {
                        break;
                    }
                    sourceIndex++;
                }
                if (stack == null) {
                    return;
                }
                // Iterate filtered pipes
                Material material = stack.getType();
                int initialSourceAmount = stack.getAmount();
                boolean acceptUnfilteredOutputs = true;
                int outPipeIndex = 0;
                while (outPipeIndex < outputPipesSize && stack.getAmount() > 0) {
                    PipeOutput out = outputs.get(outPipeIndex);
                    if (!out.isFiltered()) {
                        break;
                    }
                    if (out.passesFilter(material)) {
                        if (out.isLoadedChunk()) {
                            // Filter matches and is reacheable. Transfer items
                            stack = out.pushStack(stack, transferStackSize);
                        } else {
                            // Filter matches and is unreacheable. Refuse the transfer
                            acceptUnfilteredOutputs = false;
                        }
                    }
                    outPipeIndex++;
                }
                // Iterate unfiltered pipes
                if (acceptUnfilteredOutputs) {
                    while (outPipeIndex < outputPipesSize && stack.getAmount() > 0) {
                        PipeOutput out = outputs.get(outPipeIndex);
                        if (out.isLoadedChunk()) {
                            stack = out.pushStack(stack, transferStackSize);
                        }
                        outPipeIndex++;
                    }
                }
                // Put back the rest (if available)
                if (initialSourceAmount != stack.getAmount()) {
                    if (stack.getAmount() == 0) {
                        source.clear(sourceIndex);
                    } else {
                        source.setItem(sourceIndex, stack);
                    }
                    maxTransferStacks--;
                }
                sourceIndex++;
            }

        });
    }

    public void syncFiltersAt(Location location, Container container) {
        boolean touchedOutputs = false;
        for (PipeOutput p : outputs) {
            if (p.getLocation().equals(location)) {
                p.updateFilter(container.getInventory());
                touchedOutputs = true;
            }
        }
        for (PipeInput p : inputs) {
            if (p.getLocation().equals(location)) {
                p.updateFilter(container.getInventory());
            }
        }
        if (touchedOutputs) {
            outputs.sort(Comparator.comparingInt(po -> po.isFiltered() ? 0 : 1));
        }
    }

    public void syncPipesIf(Predicate<Pipe> predicate) {
        boolean touchedOutputs = false;
        List<PipeInput> brokenInputs = new ArrayList<>(0);
        List<PipeOutput> brokenOutputs = new ArrayList<>(0);
        for (PipeOutput p : outputs) {
            if (predicate.test(p)) {
                BlockState block = p.getLocation().getBlock().getState();
                if (block instanceof Barrel) {
                    p.updateFilter(((Barrel) block).getInventory());
                    PipeFacing facing = PipeFacing.getFromBlockData(block.getBlockData());
                    if (facing != p.getFacing()) {
                        p.setFacing(facing);
                    }
                } else {
                    brokenOutputs.add(p);
                }
                touchedOutputs = true;
            }
        }
        for (PipeInput p : inputs) {
            if (predicate.test(p)) {
                BlockState block = p.getLocation().getBlock().getState();
                if (block instanceof Barrel) {
                    p.updateFilter(((Barrel) block).getInventory());
                    PipeFacing facing = PipeFacing.getFromBlockData(block.getBlockData());
                    if (facing != p.getFacing()) {
                        p.setFacing(facing);
                    }
                } else {
                    brokenInputs.add(p);
                }
            }
        }
        if (touchedOutputs) {
            outputs.sort(Comparator.comparingInt(po -> po.isFiltered() ? 0 : 1));
        }
        outputs.removeAll(brokenOutputs);
        inputs.removeAll(brokenInputs);
    }

    public boolean removePipe(Location loc) {
        return inputs.removeIf(p -> p.getLocation().equals(loc))
                || outputs.removeIf(p -> p.getLocation().equals(loc));
    }

    public void removeAndGetPipes(Predicate<Pipe> predicate, Consumer<Pipe> consumer) {
        Predicate<Pipe> removeIf = (p) -> {
            if (predicate.test(p)) {
                consumer.accept(p);
                return true;
            }
            return false;
        };
        inputs.removeIf(removeIf);
        outputs.removeIf(removeIf);
    }

    public void forEachPipes(Consumer<Pipe> consumer) {
        inputs.forEach(consumer);
        outputs.forEach(consumer);
    }

    public Stream<Pipe> streamPipes() {
        return Stream.concat(inputs.stream(), outputs.stream());
    }

    public boolean isPipe(Location loc) {
        return inputs.stream().anyMatch(p -> p.getLocation().equals(loc))
                || outputs.stream().anyMatch(p -> p.getLocation().equals(loc));
    }

    public int size() {
        return inputs.size() + outputs.size();
    }

    public JSONObject toJSON() {
        return new JSONObject()
                .put("name", name)
                .put("transferStackCount", transferStackCount)
                .put("transferStackSize", transferStackSize)
                .put("publicVisibility", publicVisibility)
                .put("inputs", inputs.stream().map(Pipe::toJSON).collect((Supplier<JSONArray>) JSONArray::new, JSONArray::put, JSONArray::putAll))
                .put("outputs", outputs.stream().map(Pipe::toJSON).collect((Supplier<JSONArray>) JSONArray::new, JSONArray::put, JSONArray::putAll));
    }

    public static PipeGroup fromJSON(JSONObject object) {
        PipeGroup group = new PipeGroup(object.getString("name"), object.getInt("transferStackCount"), object.getInt("transferStackSize"), object.getBoolean("publicVisibility"));
        JSONArray inputs = object.getJSONArray("inputs");
        JSONArray outputs = object.getJSONArray("outputs");
        for (int i = 0; i < inputs.length(); i++) {
            try {
                group.inputs.add(PipeInput.fromJSON(inputs.getJSONObject(i), group));
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
        }
        for (int i = 0; i < outputs.length(); i++) {
            try {
                group.outputs.add(PipeOutput.fromJSON(outputs.getJSONObject(i), group));
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
        }
        group.outputs.sort(Comparator.comparingInt(po -> po.isFiltered() ? 0 : 1));
        return group;
    }

}
