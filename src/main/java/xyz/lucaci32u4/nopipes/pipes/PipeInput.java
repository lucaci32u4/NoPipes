package xyz.lucaci32u4.nopipes.pipes;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Container;
import org.bukkit.block.Furnace;
import xyz.lucaci32u4.nopipes.json.JSONObject;
import xyz.lucaci32u4.nopipes.PipeFacing;
import xyz.lucaci32u4.nopipes.WandFunction;

public class PipeInput extends Pipe {
    private static final String DISPLAY_NAME_UNFILTERED = WandFunction.INPUT.getColorString() + "input";
    private static final String DISPLAY_NAME_FILTERED = ChatColor.LIGHT_PURPLE + "filtered " + DISPLAY_NAME_UNFILTERED;

    public PipeInput(Location location, PipeFacing facing, PipeGroup group) {
        super(location, facing, group);
    }

    @Override
    public String getDisplayName() {
        return isFiltered() ? DISPLAY_NAME_FILTERED : DISPLAY_NAME_UNFILTERED;
    }

    @Override
    public Container getTargetContainer() {
        Container container = super.getTargetContainer();
        if (container instanceof Furnace) {
            return null;
        }
        return container;
    }

    public static PipeInput fromJSON(JSONObject object, PipeGroup group) {
        return Pipe.initFromJSON(PipeInput::new, object, group);
    }

}
