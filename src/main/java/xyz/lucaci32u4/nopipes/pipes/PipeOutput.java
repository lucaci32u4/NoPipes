package xyz.lucaci32u4.nopipes.pipes;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Container;
import org.bukkit.inventory.FurnaceInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import xyz.lucaci32u4.nopipes.json.JSONObject;
import xyz.lucaci32u4.nopipes.PipeFacing;
import xyz.lucaci32u4.nopipes.WandFunction;

public class PipeOutput extends Pipe {
    private static final String DISPLAY_NAME_UNFILTERED = WandFunction.OUTPUT.getColorString() + "output";
    private static final String DISPLAY_NAME_FILTERED = ChatColor.LIGHT_PURPLE + "filtered " + DISPLAY_NAME_UNFILTERED;

    public PipeOutput(Location location, PipeFacing facing, PipeGroup group) {
        super(location, facing, group);
    }

    @Override
    public String getDisplayName() {
        return isFiltered() ? DISPLAY_NAME_FILTERED : DISPLAY_NAME_UNFILTERED;
    }

    ItemStack pushStack(ItemStack initialStack, int maxItemStack) {
        Container container = getTargetContainer();
        if (container == null) {
            return initialStack;
        }
        Material material = initialStack.getType();
        int amount = initialStack.getAmount();
        Inventory inventory = container.getInventory();
        int destSize = inventory.getSize();
        if (inventory instanceof FurnaceInventory) {
            FurnaceInventory furnaceInventory = (FurnaceInventory) inventory;
            if (facing == PipeFacing.DOWN || facing.isHorizontal()) {
                ItemStack existing = facing == PipeFacing.DOWN ? furnaceInventory.getSmelting() : furnaceInventory.getFuel();
                if (existing != null && !existing.getType().isAir()) {
                    if (existing.getType() == material) {
                        int transferAmount = Math.min(amount, Math.max(0, Math.min(material.getMaxStackSize(), maxItemStack) - existing.getAmount()));
                        if (transferAmount > 0) {
                            amount -= transferAmount;
                            existing.setAmount(existing.getAmount() + transferAmount);
                            if (facing == PipeFacing.DOWN) furnaceInventory.setSmelting(existing);
                            else furnaceInventory.setFuel(existing);
                        }
                    }
                } else {
                    int transferAmount = Math.min(amount, Math.min(material.getMaxStackSize(), maxItemStack));
                    ItemStack newstack = initialStack.clone();
                    newstack.setAmount(transferAmount);
                    if (facing == PipeFacing.DOWN) furnaceInventory.setSmelting(newstack);
                    else furnaceInventory.setFuel(newstack);
                    amount -= transferAmount;
                }
            }
        } else {
            for (int i = 0; i < destSize && amount > 0; i++) {
                ItemStack stack = inventory.getItem(i);
                if ((stack == null || stack.getType().isAir())) {
                    int transferAmount = Math.min(amount, Math.min(material.getMaxStackSize(), maxItemStack));
                    ItemStack newstack = initialStack.clone();
                    newstack.setAmount(transferAmount);
                    inventory.setItem(i, newstack);
                    amount -= transferAmount;
                }
                if (stack != null && stack.getType() == material) {
                    int transferAmount = Math.min(amount, Math.max(0, Math.min(material.getMaxStackSize(), maxItemStack) - stack.getAmount()));
                    if (transferAmount > 0) {
                        amount -= transferAmount;
                        stack.setAmount(stack.getAmount() + transferAmount);
                        inventory.setItem(i, stack);
                    }
                }
            }

        }
        initialStack.setAmount(amount);
        return initialStack;
    }

    public static PipeOutput fromJSON(JSONObject object, PipeGroup group) {
        return Pipe.initFromJSON(PipeOutput::new, object, group);
    }
}
